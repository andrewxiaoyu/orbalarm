import React, { Component } from 'react';
import '../stylesheets/description.css';

class Description extends Component {
  render() {
    return (
        <div className="description">
            
            <div id="text">
                <h1> Never Miss Anything Again. </h1>
                <h2> Missed a bus stop? An important meeting? Never forget to wake up again with location and time based alarms, all in one application.  </h2>
            </div>
            <img src={require("../imgs/locationtime.png")} />
        </div>
    );
  }
}

export default Description;
