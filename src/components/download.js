import React, { Component } from 'react';
import '../stylesheets/download.css';

class Download extends Component {
  render() {
    return (
        <div className="download">
            <div id="text">
                <h1>Download</h1>
                <h2> Available soon in the Google Play and the App Store. </h2>
            </div>
        </div>
    );
  }
}

export default Download;
