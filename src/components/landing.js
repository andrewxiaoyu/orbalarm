import React, { Component } from 'react';
import '../stylesheets/landing.css';

class Landing extends Component {
  render() {
    return (
      <div className="section, landing">
        <img id="logo" src={require("../imgs/orbalarm.png")} />
        <div className="text">
          <h1> OrbAlarm </h1>
        </div>
      </div>
    );
  }
}

export default Landing;
