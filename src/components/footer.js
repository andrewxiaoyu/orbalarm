import React, { Component } from 'react';
import '../stylesheets/footer.css';

class Footer extends Component {
  render() {
    return (
        <div className="footer">
            <p> &copy; 2018 Andrew Wang. All Rights Reserved. </p>
        </div>
    );
  }
}

export default Footer;
