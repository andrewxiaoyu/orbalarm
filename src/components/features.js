import React, { Component } from 'react';
import '../stylesheets/features.css';

class Features extends Component {
  render() {
    return (
        <div className="features">
            <div className="feature">
                <h1> <span className="number"> 1 </span> Set It. </h1>
                <h2> Record a time or location and save it for later. </h2>
            </div>
            <div className="feature">
                <h1> <span className="number"> 2 </span> Forget It. </h1>
                <h2> Repeat the alarm on weekdays. Or weekends if you're that kind of guy. </h2>
            </div>
            <div className="feature">
                <h1> <span className="number"> 3 </span> Snooze It. </h1>
                <h2> Pause the alarm if you can't wake up. Don't worry, it'll ring again. </h2>
            </div>
        </div>
    );
  }
}

export default Features;
