import React, { Component } from 'react';
import './stylesheets/style.css';

import Landing from './components/landing';
import Ball from './components/ball';
import Description from './components/description';
import Features from './components/features';
import Download from './components/download';
import Footer from './components/footer';

class App extends Component {
  render() {
    return (
      <div className="app">
        <Landing />
        <Ball />
        <Description />
        <Ball />
        <Features />
        <Ball />
        <Download />
        <Footer />
      </div>
    );
  }
}

export default App;
